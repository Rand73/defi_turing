# Exercice 10 : Somme des nombres premiers compris entre 1 et 10'000'000
from iter import PrimeNumbers
if __name__ == '__main__':
    somme: int = 0
    for i in PrimeNumbers():
        if i > 10000000:
            break
        print(i)
        somme += i
    print(somme)
