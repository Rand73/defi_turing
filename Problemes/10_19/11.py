# Exercice 11 : Miroir d'un nombre où mirroir(n) = 4 * n


def reverse(s: str):
    return s[::-1]


def mirror(n: int) -> int:
    m = "{}".format(n)
    m = int(reverse(m))
    return m


if __name__ == '__main__':
    for i in range(10000000, 0, -1):
        if 4 * i == mirror(i):
            print(i)
            break
