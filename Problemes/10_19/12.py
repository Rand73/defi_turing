# Exercice 11 : Nombres triangulaires
from iter import TriangularNumbers
from math import sqrt
from typing import List


def nb_divisors(nb: int) -> int:
    result = len(divisors(nb))
    #print(result, nb)
    return result


def divisors(nb: int):
    racine = int(sqrt(nb)) + 1
    div: List[int] = []
    div2 = []
    for i in range(1, racine + 1):
        if nb % i == 0:
            div.append(i)
    for j in div:
        div2.append(int(nb / j))
    div = sorted(set(div) | set(div2))
    return div


if __name__ == '__main__':
    for i in TriangularNumbers(start=41039):
        if nb_divisors(i) >= 1000:
            print(i)
    pass
