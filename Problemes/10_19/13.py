# Exercice 13 : Deuxieme plus grand nombre carré palindrome à nombre pair


def palindrome(nb: str) -> bool:
    if nb == nb[::-1]:
        return True
    return False


if __name__ == '__main__':
    i: int = 0
    while True:
        i += 1
        nb = i
        nb_str = '{}'.format(nb ** 2)
        if palindrome(nb_str):
            if len(nb_str) % 2 == 0:
                print('{}² = {}'.format(nb, nb_str))

