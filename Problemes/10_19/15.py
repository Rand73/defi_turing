# Exercice 15 : Les nombres du Mage Hic
"""
123456789

"""

from itertools import permutations


def product(a, b, c) -> int:
    return a * b * c


def add(a, b, c=0) -> int:
    return a + b + c


def somme_of_products(grid) -> int:
    a, b, c, \
    d, e, f, \
    g, h, i = grid
    return add(
           add(product(a, b, c),
               product(d, e, f),
               product(g, h, i)),
           add(product(a, d, g),
               product(b, e, h),
               product(c, f, i)))


if __name__ == '__main__':
    max = 0
    min = 1000
    for i in permutations(range(1, 10)):
        somme = somme_of_products(i)
        if somme > max:
            max = somme
        if somme < min:
            min = somme

    print(min, max, min * max)
