# Problème 16 : Cryptarithme.
from itertools import permutations


def to_dic(i, dic):
    #print(dic)
    dic['T'], dic['H'], dic['R'], dic['E'], dic['N'], dic['I'], dic['O'], dic['S'], dic['U'], dic['F'] = i
    return dic


def text_to_int(dic, text) -> int:
    my_int = ''
    for i in text:
        my_int += str(dic[i])
    return int(my_int)


def verif(dic):
    three = 'THREE'
    nine = 'NINE'
    trois = 'TROIS'
    neuf = 'NEUF'


    three = text_to_int(dic, three)
    nine = text_to_int(dic, nine)
    trois = text_to_int(dic, trois)
    neuf = text_to_int(dic, neuf)
    if three * nine == trois * neuf and three % 9 == 0\
            and neuf % 9 == 0 and trois % 3 == 0 and nine % 3 == 0:
        print('{} * {} = {} * {} = {}'.format(three, nine, trois, neuf, three * nine))
        return True
    return False

if __name__ == '__main__':
    dic = {'T': -1,
           'H': -1,
           'R': -1,
           'E': -1,
           'N': -1,
           'I': -1,
           'O': -1,
           'S': -1,
           'U': -1,
           'F': -1}



    for i in permutations(range(0, 10)):
        dic = to_dic(i, dic)
        if verif(dic):
            break


    print(dic)
