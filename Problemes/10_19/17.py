# Exercice 17 : Les nombres amicaux.
# Trouver la somme des nombres amicaux entre 1 et 1000000.
from extend_math import clean_divisors as divisors


class Liste(list):

    def add(self):
        result = 0
        for i in self:
            result += i
        return result


if __name__ == '__main__':
    friendly_numbers: [int] = []
    for i in range(2, 100000):
        if i in friendly_numbers:
            continue
        somme_div_i = Liste(divisors(i)).add()
        #print(i, somme_div_i, divisors(i))
        if i == somme_div_i or somme_div_i > 100000:
            continue
        if i == Liste(divisors(somme_div_i)).add():
            friendly_numbers.extend([i, somme_div_i])
            print(i, somme_div_i)
    print(Liste(friendly_numbers).add())
    print(friendly_numbers)