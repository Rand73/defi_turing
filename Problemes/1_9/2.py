if __name__ == "__main__":
    prem = 0
    deux = 1
    somme_impaire = 0
    while True:
        if deux % 2 == 1:
            somme_impaire += deux
        buf = prem + deux
        prem = deux
        deux = buf
        if deux > 4000000:
            break
    print(somme_impaire)
