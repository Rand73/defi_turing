# Plus grand palndrome en multipliant un nombre à 4 chiffres avec un nombre à 3 chiffres


def reverse(s: str) -> str:
    return s[::-1]


def palindrome(s: str) -> bool:
    if s == reverse(s):
        return True
    return False


if __name__ == "__main__":
    result = 0

    for i in range(9999, 9000, -1):
        for j in range(999, 900, -1):
            r = i * j
            r_str = "{}".format(r)
            if palindrome(r_str):
                if r > result:
                    result = r
    print(result)
