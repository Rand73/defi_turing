# Somme des chiffres de 2013!
from math import factorial
if __name__ == "__main__":
    nb = 2013
    nb_fact = factorial(nb)
    somme = 0

    for i in "{}".format(nb_fact):
        somme += int(i)
    print(somme)
