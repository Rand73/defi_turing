from iter import PrimeNumbers
from itertools import islice

# Trouver le 23456eme nombre premier.

if __name__ == '__main__':
    result: int = 0
    for i in PrimeNumbers(23456):

        result = i
    print(result)
    li = list(islice(PrimeNumbers(), 23455, 23456))
    print(li)
