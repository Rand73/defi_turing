# Trouver l'aire d'un rectangle-6400 (voir enoncé sur http://turing.nymphomath.ch/probleme_membre.php?id=8)
"""
pour un rectangle de 4 * 5
nombre de carrés 2 * 2
(4 - 2 + 1) * (5 - 2 + 1) = 3 * 4 = 12
3 * 3
(4 - 3 + 1) * (5 - 3 + 1) = 2 * 3 = 6
4 * 4
(4 - 4 + 1) * (5 - 4 + 1) = 1 * 2 = 2
"""


def rectangle_area(w: int, h: int) -> int:
    return w * h


def rectangle_number(w: int, h: int) -> int:
    result: int = 0
    for i in range(w, 0, -1):
        result += (w - i + 1) * (h - i + 1)
    print("le rectangle {0}:{1} est un rectangle : {2}".format(w, h, result))
    return result


if __name__ == '__main__':
    width: int = 1
    height: int = 6

    while rectangle_number(width, height) < 6400:
        width += 1
        height += 1
