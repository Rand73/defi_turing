# Triplet pythagoricien
from typing import List
from math import sqrt

def find_triplet():
    result: List[(int, int, int)] = []

    for i in range(3, 3600):
        print('{}%'.format(i/3600*100))
        for j in range(i, 3600):
            if 3600 < i + j:
                break
            for k in range(j, int(sqrt(i ** 2 + j ** 2)) + 1):
                if 3600 < i + j + k:
                    break
                if (i ** 2 + j ** 2) == k ** 2:
                    if i + j + k == 3600:
                        result.append((i, j, k))
                        print(i, j, k)
    return result


if __name__ == '__main__':
    a = find_triplet()
    result: int = 0
    for a, b, c in a:
        print('a={}, b={}, c={}, a + b + c = {}, a * b * c = {}'.format(a, b, c, a + b + c, a * b * c))
        if a * b * c > result:
            result = a * b * c
    print(result)
    pass
