# Package math + autres fonctions et classes en rapport avec les mathématiques
# Créée le 17/02/2019
# Ajout de la fonction divisors(nb: int) -> [int]


from math import *


def divisors(nb: int) -> [int]:
    """

    :rtype: [int]
    :param nb: Entier suppérieur à 1
    :return: Liste des diviseurs de nb.
    """
    if (not isinstance(nb, int)) or nb < 1:
        return None

    racine = int(sqrt(nb)) + 1
    div = []
    div2 = []
    for i in range(1, racine + 1):
        if nb % i == 0:
            div.append(i)
    for j in div:
        div2.append(int(nb / j))
    div = sorted(set(div) | set(div2))
    return div


def clean_divisors(nb: int) -> [int]:
    """
    Liste des nombres propres
    :param nb: Entier positif suppérieur à 1
    :return: Liste des diviseurs propres de nb
    """
    result = divisors(nb)
    if result is not None:
        result.pop()
    return result


def number_of_divisors(nb: int) -> int:
    """

    :param nb:
    :return: Le nombre de diviseurs de l'entier nb
    """
    return len(divisors(nb=nb))
