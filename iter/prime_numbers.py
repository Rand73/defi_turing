from typing import List
from math import sqrt


class PrimeNumbers:
    __prime_numbers: List[int] = []
    __nb_prime_numbers: int = 1
    __nb: int = 1

    def __init__(self, nb_prime_numbers: int = -1):
        self.__nb_prime_numbers = nb_prime_numbers
        self.__prime_numbers = []
        self.__nb = 1
        pass

    def __iter__(self):
        return self

    def __next__(self):
        if len(self.__prime_numbers) > self.__nb_prime_numbers - 1 and self.__nb_prime_numbers != -1:
            raise StopIteration

        if len(self.__prime_numbers) == 0:
            self.__prime_numbers.append(2)
            return 2

        while True:
            self.__nb += 2
            if self.check_prime_number():
                self.__prime_numbers.append(self.__nb)
                return self.__nb

    def check_prime_number(self) -> bool:
        for pn in self.__prime_numbers:
            if pn > int(sqrt(self.__nb)) + 1:
                return True
            if self.__nb % pn == 0:
                return False
        return True


# Test
if __name__ == '__main__':
    for i in PrimeNumbers(23456):
        print(i)
