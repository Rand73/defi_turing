# Classe iterrable SyracuseSuite : génére toute la suite de syracuse d'un nombre entier entré.


class SyracuseSuite:
    __number: int
    __start: bool

    def __init__(self, number: int):
        self.__number = number
        self.__start = True
        return

    def __iter__(self):
        return self

    def __next__(self) -> int:
        if self.__start:
            self.__start = False
            return self.__number

        if self.__number == 1:
            raise StopIteration

        if self.__number % 2 == 0:
            self.__number = int(self.__number / 2)
            return self.__number

        self.__number = self.__number * 3 + 1
        return self.__number

    pass


# Test

if __name__ == '__main__':
    for i, j in enumerate(SyracuseSuite(14)):
        print(j)

    print(i+1)