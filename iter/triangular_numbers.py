# Classe itrérable TriangularNumbers


class TriangularNumbers:
    __nb: int
    __triangular_number: int
    __max: int
    __start: bool

    def __init__(self, nb_max: int = -1, start: int = 0):
        self.__triangular_number = 0
        self.__nb = start
        self.__max = nb_max - 1
        if start > 0:
            self.__start = True
        else:
            self.__start = False
        pass

    def __iter__(self):
        return self

    def __next__(self):
        if self.__nb > self.__max > -1:
            raise StopIteration
        self.__nb += 1
        if self.__start:
            self.__start = False
            for i in range(self.__nb):
                self.__triangular_number += i + 1
            return self.__triangular_number
        self.__triangular_number += self.__nb
        return self.__triangular_number


# Test

if __name__ == '__main__':
    for i in TriangularNumbers():
        print(i)
        if i > 100000:
            break
